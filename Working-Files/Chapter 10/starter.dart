import 'dart:isolate';

void playHideAndSeekTheLongVersion(SendPort sendPort) {
  var counting = 0;
  for (var i = 1; i <= 1000000000; i++) {
    counting = i;
  }
  sendPort.send('$counting! Ready or not, here I come!');
}

Future<void> main() async {
  // 1 - Create a place to recieve data
  final receivePort = ReceivePort();

  // 2 - Create the isolate that will send the data to our reciever
  final isolate = await Isolate.spawn(
    playHideAndSeekTheLongVersion,
    // 3
    receivePort.sendPort,
  );

  // 4 - Create a listener to get the results from our isloate declared above.

  receivePort.listen((message) {
    print(message);
    // 5 - Clo0se the reciever port and kill the isolate!

    receivePort.close();
    isolate.kill();
  });
}
