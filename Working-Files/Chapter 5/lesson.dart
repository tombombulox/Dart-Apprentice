void main() {
  // Our Greetings Function

  print(myNameIs("DJ"));
  print(generateCustomGreeting("DJ", "K", "Mr.", "29", "IT Professional"));
  print(yetAnotherfunction("DJ", true, 69));
  print(additionFunction(firstNumber: 4, secondNumber: 2));
  print(isStrictFunction(firstName: "DJ", myAge: 29, lastName: "Kemmet"));

  double taxRate = .0825;
  double costOfProduct = 9.99;

  Function totalCostOfProduct =
      ({required double taxRate, required double costOfProduct}) {
    return (costOfProduct * taxRate) + costOfProduct;
  };

  print(totalCostOfProduct(taxRate: taxRate, costOfProduct: costOfProduct));

  String tellMeThePrice({required Function totalCostOfProduct}) {
    return "THE PRICE IS ${totalCostOfProduct}";
  }

  // print(tellMeThePrice(
  //     totalCostOfProduct: totalCostOfProduct(
  //         taxRate: taxRate, totalCostOfProduct: costOfProduct)));

  num calculateTax(num productCost, num taxRate) => productCost * taxRate;
  print(calculateTax(9.99, .0825));
}

String isStrictFunction({
  int myAge = 0,
  required String firstName,
  String lastName = "",
}) {
  return "$firstName $lastName is $myAge";
}

num additionFunction({firstNumber, secondNumber}) {
  return firstNumber + secondNumber;
}

String yetAnotherfunction(String myName, [myAge, int myAgeAgain = 22]) {
  return "Hello I am $myName and myage = $myAge and myAgeAgain is $myAgeAgain";
}

String myNameIs(String name) {
  return "Hello $name";
}

String generateCustomGreeting(
  /// Dynamically generates a greeting based of required and
  /// optional parameters by generating the greeting in the order
  /// the parameters would be used.
  String firstName,
  String lastName, [
  String? title,
  String? age,
  String? profession,
]) {
  String Message = "Hello";

  /* if they don't want to use a title, 
  don't add it.  */
  if (title != null) {
    Message += " $title";
  }

  // The first and last name are required so we can safely add them.
  Message += "$firstName $lastName!";

  if (age != null) {
    Message += " I see here you are 29";
  }

  if (profession != null) {
    Message += " and are a $profession!";
  } else {
    Message += "!";
  }

  return "$Message";
}

num additionFunctionTwo({required firstNumber, required secondNumber}) {
  return firstNumber + secondNumber;
}

String customGreeting(
  String name,
) {
  return "hello!";
}
