void main() {
  print(isPositive(3));
  print(isPositive(-1));
  //print(isPositive(null));

  // My experiment just to make sure I remember
  // how to use ?, My editor tells me this is bad news.
  // Obviously because it goes against best practices.
  int? myNullVariable = null;
  // wait, what it's saying is don't do the ' = null;' bit. Leave
  // leave it up to the compiler to handle that.
  print('The value of myNullVariable is: $myNullVariable');

  int? age;
  double? height;
  String? message;

  print(age);
  print(height);
  print(message);

  String? profession;
  print(profession);
  print('The type of profession is: ${profession.runtimeType}');
  profession = "Basketball Player";
  print(
      'Now that I have explicitly set it to a string, profession\'s type is: ${profession.runtimeType}');
  profession = 69.toString();

  //const iLove = 'Dart';

  String? name;
  name = 'DJ';
  print(name.length);

  String? myGreeting; // null
  myGreeting ??= 'Hello, World'; // since it's null make it "hello World"
  print(myGreeting); // print so i can see the results
  myGreeting = 'Aloha, World'; // explicitly set the value
  print(myGreeting); // I can see the new greeting and this was never null!

  bool flowerIsBeautiful = isBeautiful('flower')!;
  print(flowerIsBeautiful);

  Person DJ = Person(firstName: 'DJ', lastName: 'K', age: 29);

  print('My Favorite number is unset so it should be 69: ${DJ.favoriteNumber}');
  DJ.setFavoriteNumber = 55;
  print(
      'Now my favorite number is set and should not be 69: ${DJ.favoriteNumber}');

  List<int>? myList = [1, 2, 3];
  myList.forEach((element) {
    print(element);
  });
  myList = null;

  int? myItem = myList?[2];
  print(myItem);

  // It's null!
  //myList.forEach((element){print(element);};
}

bool isPositive(int anInteger) {
  if (anInteger == null) {
    return false;
  } else {
    return !anInteger.isNegative;
  }
}

bool? isBeautiful(String? item) {
  if (item == 'flower') {
    return true;
  } else if (item == 'garbage') {
    return false;
  }
  return null;
}

// Wait a second, do i remember how to write a class and
// create an itilaizer using Cascade Notation?

class Person {
  //Unnamed Constructor with required, named parameters that set the private values.
  Person(
      /* 
    using optional named parameters to avoid incorrect orders with {} and then
    giving the caller the option to set favorite number. This create a type 
    safety issue I think. 
    */
      {
    required String firstName,
    required String lastName,
    required int age,
    int? favoriteNumber, // You don't HAVE TO set this optional named parameter
  })  : _firstName = firstName,
        _lastName = lastName,
        _age = age;

  // Once you set these they cannot be changed and a private to the class.
  // DOT CONNECTED - If It's final, It's 'requied'
  final String _firstName;
  final String _lastName;
  final int _age;
  int? _favoriteNumber;

  String get firstName => _firstName;
  String get lastName => _lastName;
  int get age => _age;

  /*
    So Applying what I've learned in this chapter. I can get or set
    a the optional, null field favoriteNumber. if I choose to 'get' it and
    it is null then I want to return the value 69. See above for an example. 
  */
  set setFavoriteNumber(int FavoriteNumber) => _favoriteNumber = FavoriteNumber;
  int get favoriteNumber => _favoriteNumber ??= 69;
}

//YAP. Stoked i can piece these things together on my own.