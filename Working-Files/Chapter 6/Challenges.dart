class Student {
  //The solution is to use named parameters in the class signature (our unnamed
  // constructor) which are NOT final and therefore can be set when you instantiate
  // your class, you then take those values and then using cascade notation
  // set the value of your final, library private variables with the
  // assignable vartiable values. So conceptually you're passing puublic variables
  // into the class to be used as the value of the class-specific, private
  // variables.
  Student({String firstName = '', String lastName = '', int grade = 0})
      : _firstName = firstName,
        _lastName = lastName,
        grade = grade; // Forgot the ; When coding.

  // Class Properties (fields)
  final String _firstName;
  final String _lastName;
  int grade = 0;

  @override
  String toString() {
    String studentObjectAsString =
        "Student( firstname: $_firstName, lastName : $_lastName, grade : $grade";
    return studentObjectAsString;
  }
}

class Sphere {
  // Constructor
  const Sphere({required int radius})
      : assert(radius >= 0),
        _radius = radius;

  // Properties
  final int _radius;
  // These are static because they're like a property of
  // a class that exist regardless of if there is a radius or not,
  // the values would just be zero. Also, this stops us from needing to
  // make them final and define them.
  static num _volume = 4; // Whatever the formula is to compute based
  static num _surfaceArea = 3; // Whatever the formula is to compute based

  get vol => _volume;
  get surfaceA => _surfaceArea;
}
