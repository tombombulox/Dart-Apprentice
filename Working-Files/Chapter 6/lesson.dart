import 'user.dart';

void main() {
  const user = User(id: 42, name: 'DJ');
  // Get the object's toString function
  print(user);

  // Access the object's properties.
  print("${user.name}");

  // creating an instance of an anonymous user
  // bas calling the named constructor for it as part of the
  // class.
  const anonymous = User.anonymous();

  // Referencing it's properties
  print("${anonymous.name}");

  final vicki = User(id: 24, name: 'Vicki');
  // vicki._name = 'Nefarious hacker';
  print(vicki);

  final email = Email(value: 45);
  email.value = 'my@email.com';
  final emailString = email.value;

  print(User.anonymous());
  print(User); //demonstrates our default values using static and const
}
