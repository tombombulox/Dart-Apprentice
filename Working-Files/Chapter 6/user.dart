class User {
  // before moving on, this makes an immutable instance of the user class
  const User({this.id = _anonymousId, this.name = _anonymousName})
      : assert(id >= 0);

  const User.anonymous() : this();

  // Once set, these do not change, they're final and facilitate const.
  // These are now public variables, but they're final so we don't need to
  // write getters and setters.
  final String name;
  final int id;

  static const _anonymousId = 0;
  static const _anonymousName = 'anonymous';

  String toJson() {
    return '{"id": $id, "name": "$name"}';
  }

  // overriding the default toString method that comes with the
  // object class to return our object properties in a human readable way.
  @override
  String toString() {
    return 'User(id: $id, name: $name)';
  }
}

class Email {
  Email({final value});

  // Named constructor practice with named parameters
  Email.strictEntry({String value = '', bool subStatus = false});

  // class Properties.
  String value = '';

  // My stab at applying the concepts.
  bool _subStatus = false;

  bool get substatus => _subStatus;
  set substatus(bool substatus) => _subStatus = substatus;
}
