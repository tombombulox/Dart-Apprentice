const global = 'Hello, world';

enum Weather {
  sunny,
  snowy,
  cloudy,
  rainy,
}

main() {
  const local = 'Hello, main';

  const bool isTrue = true;
  const bool isFalse = false;

  /* to really fuss with concepts I'm combinig late (not defined yet)
  with final (permanent once defined) just because.return*/
  late final permanentBoolNotAssigned = 3 < 1;

  print("Truth is $isTrue, and lies are $isFalse");
  print(
      "This was unncessary but I learned the order of these things: $permanentBoolNotAssigned");

  // TESTING EQUALITY

  const bool doesOneEqualTwo = (1 == 2);
  // False
  const bool doesOneEqualOne = (1 == 1);
  // True
  const bool doesOneEqualStringOne = (1 == '1');
  // False

  print("$doesOneEqualTwo, $doesOneEqualOne, $doesOneEqualStringOne");

  //
  // TESTING INEQUALITY
  //

  const bool doesOneEqualTwoB = (1 != 2);
  // True, could also be !(1 == 2)

  const bool doesOneEqualOneB = (1 != 1);
  // False, could also be !(1 == 1)

  const bool doesOneEqualStringOneB = (1 != '1');
  // True, could also be !(1 == '1')

  print("$doesOneEqualTwoB, $doesOneEqualOneB, $doesOneEqualStringOneB");

  //
  // TESTING GREATER AND LESS THAN
  //

  const bool isOneGreaterThanZero = (1 > 0);
  // True

  const bool isOneLessThanTwo = (1 < 2);
  // True

  const bool oneGreatThanOrEqualZero = (1 >= 0);
  // True

  const bool twoIsLessThanOrQualThree = (2 <= 3);
  // True

  print(
      "$isOneGreaterThanZero, $isOneLessThanTwo, $oneGreatThanOrEqualZero, $twoIsLessThanOrQualThree");

  //
  // AND OPERATOR
  //
  const bool seatbeltIsOn = true;
  const bool keyInIgnition = false;
  const bool carIsInPark = true;

  const bool userReadyForDrive = seatbeltIsOn && keyInIgnition;
  const bool userIsReallyReadyToDrive =
      seatbeltIsOn && keyInIgnition && carIsInPark;

  if (userReadyForDrive == true) {
    print("Start Your Engines");
  } else {
    print("You should check the key and your seat belt");
    print("One condition was flase");
  }

  if (userIsReallyReadyToDrive == true) {
    print("You are really ready to drive");
  } else {
    print("You've got driving problems bud.");
  }

  const bool wantsCandy = true;
  const bool hasMoneyForCandy = true;
  const bool currentlyHasCandy = false;
  const bool canEatCandy = wantsCandy && hasMoneyForCandy || currentlyHasCandy;

  print("Can I get Candy?: $canEatCandy");
  /* yes because I WANT candy (true) and while I dont have candy right now 
  (currentlyHasCandy is false) I have the money for candy (hasMoneyForCandy is true)
  so I am capable of getting candy. if wants candy was false the expression would
  evaluate to false because there would be a 'false' on both sides of the expression
  for example:
  */

  const bool wantsCandyB = false;
  const bool hasMoneyForCandyB = true;
  const bool currentlyHasCandyB = false;
  const bool canEatCandyB =
      wantsCandyB && hasMoneyForCandyB || currentlyHasCandyB;
  //false            // true              // false
  /// one side of AND is false, so it IS false and not evaluating
  /// the OR after the AND expression
  print("What about now, can I get candy?: $canEatCandyB");
  // False, No. Notice

  const String myWord = 'heavens';
  const bool isItMyWord = ('Oh Dear!' == myWord);
  print("SURVEY SAYS: $isItMyWord");

  //
  // if (<Boolean Condition>){
  //    <code to run if true>
  // }
  //
  if (2 > 1) {
    print("Two is greater than one");
  } else {
    print("The expression was false so this code ran.");
  }

  if (2 > 1) {
    const insideIf = 'Hello, anybody?';

    print(global);
    print(local);
    print(insideIf);
  }

  print(global);
  print(local);
  // print(insideIf); // Not allowed!

  const myScore = 72;
  const message = (myScore > 70) ? "You passed!" : "You failed";
  print(message);

  /*
  const <variable> = (<boolean expression>) ? <DO_IF_TRUE> : <DO_IF_FALSE>;
  */

  const int testResult = 85;
  const communicatedResult = (testResult > 70)
      ? {
          if (testResult > 80) {"You made a B!"} else {"You passed!"}
        }
      : {"You failed."};
  print(communicatedResult);

  //
  // SWITCHING NUMBERS
  //
  switch (3) {
    case 1:
      print("Your number is 1");
      break;
    case 2:
      print("Your number is 2");
      break;
    case 3:
      print("Your number is 3");
      break;
    case 4:
      print("Your number is 4");
      break;
    default:
      print("I have no idea what your number is.");
      break;
  }

  //
  // SWITCHING STRINGS
  //
  const String Pet = 'Dog';
  switch (Pet) {
    case 'Dog':
      print("You have a Dog");
      break;
    case 'Cat':
      print("You have a cat");
      break;
    case 'reptile':
      print("Gross, you have a reptile");
      break;
    default:
      print("You have a $Pet");
      break;
  }

  //
  // SWITCHING ENUMS
  //
  const weatherToday = Weather.cloudy;
  switch (weatherToday) {
    case Weather.sunny:
      print("Put on sunscreen");
      break;
    case Weather.snowy:
      print("Get your skis");
      break;
    case Weather.cloudy:
    case Weather.rainy:
      print("Bring an umbrella.");
      break;
  }

  print(weatherToday);
  // Weather.cloudy

  final index = weatherToday.index;
  print(index);
  // 2

  //
  // WHILE LOOP
  //

  int x = 0;
  bool myVar = true;
  while (myVar) {
    if (x < 4) {
      x++;
      print(x);
    } else {
      print("$x is not less than 4");
      myVar = false;
    }
  }

  //
  // DO-WHILE LOOP
  //

  int sum = 1;
  do {
    sum += 4;
    print(sum);
  } while (sum < 10);

  for (var i = 0; i < 5; i++) {
    print(i);
  }

  /*
  for(initialization; condition; action){
    do the thing here. 
  }
  */

  for (var i = 0; i < 10; i++) {
    if ((i % 2) != 0) {
      continue;
    } else {
      print("$i is an even number");
    }
  }

  const myString = 'I ❤ Dart';

  for (var codePoint in myString.runes) {
    // you're making a new string from a charcode definied by the
    // expression in the for loop.
    print(String.fromCharCode(codePoint));
  }

  const myNumbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  myNumbers.forEach((element) {
    print("The current number is $element");
  });
}
