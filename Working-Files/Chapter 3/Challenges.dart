import 'dart:math'; //provides sqrt();

main() {
  print("CHALLENGE 1");
  int myAge = 29;
  int dogs = 1;
  dogs += 1;
  print(dogs);

  print("CHALLENGE 2");
  int age = 16;
  print(age);
  age = 30;
  print("${age} \n\n");

  // CHALLENGE 2
  const x = 46;
  const y = 10;

  print("CHALLENGE 3");

  const answer1 = (x * 100) + y; // 460
  const answer2 = (x * 100) + (y * 100); //5600
  const answer3 = (x * 100) + (y / 10); //4601

  print(
      """Eqation 1 equals: ${answer1} \nEqaution 2 is: ${answer2} \nAnd 3 is ${answer3}\n\n""");

  print("CHALLENGE 4");
  const double rating1 = 7.7;
  const double rating2 = 9.9;
  const double rating3 = 6.9;

  const double averageRating = (rating1 + rating2 + rating3) / 3;
  print("The average of $rating1, $rating2, $rating3 is $averageRating \n\n");

  print("CHALLENGE 5");
  const double a = 3.14;
  const double b = 200.69;
  const double c = 12.6186;

/*
  I'm interpreting ± to mean i can decide if it's plus or minus
  since that appears to be an illegal character. also I'm cheating
  by assuming 
*/

  final double root1 = (-b + sqrt(pow(b, 2) - 4 * a * c)) / (2 * a);
  final double root2 = (-b - sqrt(pow(b, 2) - 4 * a * c)) / (2 * a);
  print("The answer to challenge 5 is: root1 = $root1 and root2 = $root2");
}
