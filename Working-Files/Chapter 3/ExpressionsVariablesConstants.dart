class Dog {
  late String Name;
  late String Breed;
  late int Age;
  late int TreatCount;

  speak() {
    if (TreatCount > 1) {
      print("Hello My name is $Name");
      TreatCount -= 1;
    }
  }

  eatTreat(int NumberOfTreats) {
    TreatCount += NumberOfTreats;
  }
}

main() {
  // This is a single line code.
  print("And this is not.");

  String ThisIsAStatement() {
    /// ThisIsAStatement returns a string letting you know it's a
    /// statement.

    // String-type Statement declares a string and sets its value
    String Statement = "This is a statement";

    /*
      and then we return the statement to whatever called this function
      because that's what we're supposed to do.
    */
    return Statement;
  }

  print(ThisIsAStatement());

/*
   Contrived example of using print statements for debugging.
*/
  bool GreaterThenOrEqualToOne(int MyNumber) {
    if (MyNumber > 1) {
      print("User passed $MyNumber which is greater than 1");
      return true;
    } else {
      // Check if the number IS 1, return true if so.
      if (MyNumber == 1) {
        print('user passed $MyNumber which is ACTUALLY 1, return true');
        return true;
      }
      // The number wasn't 1...
      else {
        print("User passed was $MyNumber which is less than 1");
        return false;
      }
    }
  }

  print(GreaterThenOrEqualToOne(12));

  // STATEMENTS

  bool isThisAStatement() {
    return true;
  }

  print(isThisAStatement);
  print("This was also a statement");

  Dog MyDog = new Dog();
  MyDog.Name = "Amira";
  MyDog.Breed = "Retriever Mix";
  MyDog.Age = 9;
  MyDog.TreatCount = 2;

  // Expressing the value of my dog's name and age.
  print("My Dog's name is ${MyDog.Name} and she is ${MyDog.Age}");

  // and expression for how many
  print("My Dog Got ${MyDog.Age / MyDog.TreatCount} treats");

  print(10 / 3);

  /*
  10 / 3 is 3.3333... since we're using the ~/ operator the decminal is removed.
  */
  print("10 ~/ 3: ${10 ~/ 3}");

  /* 
  3 goes into 11 3 times with a remainder of two therefore this returns 2
  */
  print("11 % 3: ${11 % 3}");

  /* 4 goes into 11 2 times with 3 reminaing therefore this returns 3
  */
  print("11 % 4: ${11 % 4}");

  double pi = 3.14;

  // a lot of constants and what not that i put in my blog.
  //
  //
  } //END MAIN();
