void main() {
  final jon = Person('John', 'Snow');
  final jane = Student('Jane', 'Snow');

  print(jon.fullName);
  print(jane.fullName);

  final historyGrade = Grade.B;
  jane.grades.add(historyGrade);

  final jessie = SchoolBandMember('Jessie', 'Jones');
  final marty = StudentAthlete('marty', 'McFly');

// on thing that's interesting is that I can use
// a type for the list of person and it will still work
  /// I'm guessing because They are either a person object
  /// OR they inherit from / are a child of that object type.
  List students = <Person>[jane, jessie, marty];

  students.forEach((student) {
    print("${student.fullName} is a ${student.runtimeType}");
  }); //that ends the foreach. got it.

  final PlatypusCharlie = Platypus();
  print(PlatypusCharlie.isAlive);
  PlatypusCharlie.eat();
  PlatypusCharlie.move();
  PlatypusCharlie.layEggs();
  print(PlatypusCharlie);

  final DataRepository repository = FakeWebServer();
  final temp = repository.fetchTemperature('sachse');
  print(temp);

  final platypus = Platypus();
  final robin = Robin();
  platypus.layEggs();
  robin.layEggs();
}
//
// END MAIN
//

// why am I not familiar with this?
enum Grade { A, B, C, D, F }

class Person {
  Person(this.givenName, this.surname);

  String givenName;
  String surname;
  String get fullName => '$givenName $surname';

  @override
  String toString() => fullName;
}

class Student extends Person {
  Student(String givenName, String surname) : super(givenName, surname);

  var grades = <Grade>[];

  @override
  String get fullName => '$surname $givenName';
}

class SchoolBandMember extends Student {
  SchoolBandMember(String givenName, String surname)
      : super(givenName, surname);
  static const minimumPractivetime = 2;
}

class StudentAthlete extends Student {
  StudentAthlete(String givenName, String surname) : super(givenName, surname);

  bool get isEligible => grades.every((grade) => grade != Grade.F);
}

abstract class Animal {
  bool isAlive = true;
  void eat();
  void move();

  @override
  String toString() {
    return "I'm a $runtimeType";
  }
}

class Platypus extends Animal with EggLayer {
  @override
  void eat() {
    print('nom nom nom nom');
  }

  @override
  void move() {
    print('Scuttle Scuttle');
  }

  void layEggs() {
    print("Plop Plop");
  }
}

abstract class DataRepository {
  // IT IS a constructor.
  factory DataRepository() => FakeWebServer();
  double? fetchTemperature(String City);
}

// So this is a subclass of DataRepository since
// implements it. so when we initialize it above
// we're returning this subclass in place of the abstract
// class, since FakeWebServer is a concrete class,
// and therefore callable? Idk I'm holding on by the seems
// of muh pats.
class FakeWebServer implements DataRepository {
  @override
  double? fetchTemperature(String city) {
    return 42;
  }
}

mixin EggLayer {
  void layEggs() {
    print('plop plop');
  }
}

mixin Flyer {
  void fly() {
    print('swoosh swoosh');
  }
}

abstract class Bird {
  void fly();
  void layEggs();
}

class Robin extends Bird with EggLayer, Flyer {}

extension on String {
  String get encoded {
    return _code(1);
  }

  String get decoded {
    return _code(-1);
  }

  String _code(int step) {
    final output = StringBuffer();

    for (final codePoint in runes) {
      output.writeCharCode(codePoint + step);
    }

    return output.toString();
  }
}
