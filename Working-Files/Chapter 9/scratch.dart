import 'dart:io';

String encode(String input) {
  // https://api.dart.dev/stable/2.10.4/dart-core/StringBuffer-class.html
  final output = StringBuffer();

  for (final codepoint in input.runes) {
    output.writeCharCode(codepoint + 1);
  }

  return output.toString();
}

main() {
  final original = "Test";
  final secret = encode(original);
  print(secret);
}
