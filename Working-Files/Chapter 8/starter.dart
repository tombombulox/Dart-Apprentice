import 'dart:collection';
import 'dart:math';

main() {
  var snacks = ['KitKats', 'Pringles', 'CAKE'];
  snacks.forEach((element) {
    print('$element');
  });
  print(" 'reassigning list");
  snacks = [];
  if (snacks.isEmpty) {
    print('I have no snaks');
  }

  final lifetimeSupplyOfSnacks = ['Reese', 'AirHeads', 'Pizza'];

  lifetimeSupplyOfSnacks.forEach((element) {
    print('$element');
  });

  //lifetimeSupplyOfSnacks = [];

  var theListOfAnything = ['A word', 44, true, (6 * 3 / 2)];
  theListOfAnything.forEach((element) {
    print(
        'The value of index ${theListOfAnything.lastIndexOf(element)} is $element');
  });

  var desserts = <String>['cookies', 'cupcakes', 'donuts', 'pie'];
  print(desserts);

  var snacks2 = <String>['rojos', 'Skittles', 'Trolli', 'Haribo'];
  final secondElement = snacks2[1];
  print(secondElement); // nomnoms

  final indexByValue = snacks2.indexOf('Skittles');
  print(indexByValue);

  // Setting
  snacks2[3] = 'Yommy NomNoms';
  print(snacks2[3]);

  // Adding
  snacks2.add('Choco WimWams');
  print(snacks2);

  // Removing
  snacks2.remove('Trolli');
  print(snacks2);

  // NEEYAOO LETS KICK IT UP A NYOTCH.
  Random random = Random();
  var myRandomNumber = random.nextInt(snacks2.length);

  print('This time around I got: ${snacks2[myRandomNumber]}');

  final noOtherSnackralidge = [
    'JuJu Beans',
    ' Anything Cinnamon and Liquorice (?),',
    'Mint Anything'
  ];
  //noOtherSnackralidge = []; //wanring about finality

  const myFirstList = ['toaster', 'amira', 'poptarts'];
  const mySecondList = ['hellur', 'another', 'word'];
  const myFinalList = ['what', ...myFirstList, ...mySecondList];

  print(myFinalList);

  const onMotorcycle = true;

  const thingsIShouldHaveOnMe = [
    'Wallet',
    'Glasses',
    'Watch',
    if (onMotorcycle) 'Helmet',
    if (onMotorcycle) 'eBag',
    if (onMotorcycle) 'Cardo'
  ];

  print(thingsIShouldHaveOnMe);

  const camelCaseWords = ['AnD', 'ShARp', 'OBJecTs'];
  dynamic allUppercaseWords = [
    'LOUD',
    'NOISES',
    for (var word in camelCaseWords) word.toUpperCase(),
  ];

  print(allUppercaseWords);

  var mySet = {1, 3, 5, 'Car', false, 6.9};
  print(mySet.contains(1)); //true
  print(mySet.contains('Car')); //true
  print(mySet.contains(false)); //true
  mySet.addAll([1, 2, 3, 4]);
  print(mySet);

  var setA = {1, 2, 3, 4, 5, 6};
  var setB = {4, 5, 6, 7, 8, 9};
  print(setA.intersection(setB));
  print(setA.union(setB));

  Map<String, dynamic> emptyMap = {};

  final inventory = {'cakes': 44, 'pies': 1, 'donuts': 2, 'cookies': 4};
  print(inventory['cookies']); // 4

  final treasureMap = {
    'garbage': ['in dumpster'],
    'glasses': 'on your head',
    'gold': ['in the cave', 'under your mattress', 'in that them hills']
  };
  print(treasureMap[
      'gold']); // [in the cave, under your mattress, in that them hills]

  var myList = treasureMap['gold'];

  print(treasureMap.containsKey('secret')); //false
  treasureMap['secret'] = 'you know it\'s wrong';
  print(treasureMap.containsKey('secret')); //true
  print(myList.runtimeType); // list

  treasureMap.remove('secret');
  print(treasureMap.containsKey('secret'));
}
